<?php

namespace wishlist\view;

class VueConnexion
{
    
    /**
     * Fonction permettant de rendre la vue confirmation.
     *
     */
    public function render()
    {

        $app = \Slim\Slim::getInstance();
        $urlHome = $app->urlFor('route_home');
        $urlPPerso = $app->urlFor('route_post_pagePerso');
        $urlListeCreer = $app->urlFor('route_listeCreer');

        $header = VueGenerale::renderHeader();
        $html = <<<END
        $header
<body id="connexion">

    <a href="$urlHome"><img style="height:200px;width:200px" src="$urlHome/img/logo.png"></a>


        <div>
            <form method="post" action="$urlPPerso">
                <h1>Se connecter</h1>
                <input type="email" placeholder="Mail" name="Mail" required autofocus><br>
                <input type="password" placeholder="Mot de passe" name="Passe" required ><br>
                <input class="bouton" type="submit" value="Valider" required ></input>
            </form>
        </div>
        <br>
        <br>
        <div>
            <h1>Vous n'avez pas de compte ?</h1>      
        </div>
<br>
<div>
<a href="$urlHome" class="bouton">S'inscrire</a>
<br>
</div>
<body>
END;
        echo $html;
    }
}
