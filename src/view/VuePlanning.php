<?php

namespace wishlist\view;

use wishList\model\User;

class VuePlanning
{

    /**
     * Fonction permettant de rendre la vue de page personnelle.
     *
     * @param $u Utilisateur Utilisateur connecté.
     * @param $message String Message de la liste.
     *
     */
    public function render($u, $message = "")
    {

        $app = \Slim\Slim::getInstance();
        $urlHome = $app->urlFor('route_home');
        $urlListeCreer = $app->urlFor('route_listeCreer');
        $urlPlanning = $app->urlFor('route_planning');
        $header = VueGenerale::renderHeader();


        $listes = $u->listes()->get();

        $listesTxt = "";


        foreach ($listes as $key => $value) {

            $items = $value->item()->get();

            $compteur = count($items);
            $reserve = 0;

            foreach ($items as $key1 => $value1) {

                if (!is_null($value1->reservation)) {
                    $reserve++;
                }
            }

            $urlDetailListe = $app->urlFor('route_liste', ['no' => $value->no, 'token_visu' => $value->token_visu]);
            $urlValiderListe = $app->urlFor('route_listeValider', ['no' => $value->no, 'token' => $value->token]);
            $urlModifListe = $app->urlFor('route_get_modifListe', ['no' => $value->no, 'token' => $value->token]);
            $urlHome = $app->urlFor('route_home');
            $urlListeCreer = $app->urlFor('route_listeCreer');

            if ($value->token_visu != "") {
                $visuListe = "<a  class='bouton' href='$urlDetailListe'>Liste 🔗</a>";
            } else {
                $visuListe = "<a  href='$urlValiderListe'>Valider la liste ✅</a>";
            }

            if ($compteur == 0) {
                $description = "<a href='$urlModifListe'>Aucun item dans cette liste, ajoutez en ici</a>";
                $label =
                    "<label>
                    $description
                </label>";
            } else {
                $itReserv = $reserve > 1 ? "Items réservés" : "Item réservé";
                $label =
                    "<label>$itReserv ($reserve / $compteur)<br>
                    <progress  name='prog' max='$compteur' value='$reserve'></progress>
                </label>";
            }

            $listesTxt .= " <div class='info'>
                                <span >Liste n°$value->no</span>
                                <a  href='$urlModifListe'>Modification 🖉</a>

                                $visuListe

                                $label

                             </div>";
        }

        if (isset($_SESSION['session']['user_id'])) {
            $u = User::where('user_id', '=', $_SESSION['session']['user_id'])->first();
            $participe = $u->aParticipe()->get();
            $participations = "";
            foreach ($participe as $value) {

                $urlItem = $app->urlFor('route_itemID', ['id' => $value->id]);
                $participations .= "<div class='itemReserve'><a href='$urlItem'>$value->nom</a></div>";
            }
        }

        $navBarre = VueGenerale::renderNavBarre();
        //TODO
        //Permettre les modifs de nom d'utilisateur, de mail, de mdp

        if ($message != "") {
            $message =
                "<section id='message'>$message</section>";
        }

        $nbListes = count($listes);

        $u = User::where('user_id', '=', $_SESSION['session']['user_id'])->first();
        if (empty($u->img))
            $img = "profil.png";
        else
            $img = $u->img;

        $urlHome = $app->urlFor('route_home');
        $urlPPersoModif = $app->urlFor('route_pagePersoModifier');
        $urlPPersoSupprimer = $app->urlFor('route_pagePersoSupprimer');
        $urlListePublique = $app->urlFor('route_listePublique');
        $urlCreateurs = $app->urlFor('route_createurs');
        $urlItems = $app->urlFor('route_item');

        $urlListeAjoutParToken = $app->urlFor('route_listeAjoutParToken');

        $html = <<<END

        $header
<body id="accueil">
$navBarre
<section id="mainContent">
<H1><FONT COLOR="DARKCYAN"><CENTER>Planning</FONT></H1></section>
<br>
<center><table border="2" cellspacing="3" align="center">
<tr>
 <td align="center">
 <td>8:00-9:00
 <td>9:00-10:00
 <td>10:00-11:00
 <td>11:00-12:00
 <td>14:00-15:00
 <td>15:00-16:00
 <td>16:00-17:00
</tr>
<tr>
 <td align="center">LUNDI
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><td align="center"><font color="blue"><br>
 <td align="center"><font color="pink"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="red"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="maroon"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="brown"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
</tr>
<tr>
 <td align="center">MARDI
 <td align="center"><font color="blue"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="red"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="pink"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
 <td align="center"><font color="orange"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><BR>
 <td align="center"><font color="maroon"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center">
</tr>
<tr>
 <td align="center">MERCREDI
 <td align="center"><br><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="5">
 <td align="center"><font color="brown"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="orange"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><BR>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
 <td align="center"><font color="blue"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="red"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
</tr>
<tr>
 <td align="center">JEUDI
 <td align="center"><br><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="5">
 <td align="center"><font color="brown"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="orange"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><BR>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
 <td align="center"><font color="blue"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="red"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
</tr>
<tr>
 <td align="center">VENDREDI
 <td align="center"><font color="orange"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><BR>
 <td align="center"><font color="maroon"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="blue"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
 <td align="center"><font color="pink"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="brown"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
</tr>
<tr>
 <td align="center">SAMEDI
 <td align="center"><font color="red"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td colspan="3" align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
 <td align="center"><font color="pink"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="brown"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
</tr>
<tr>
<td align="center">DIMANCHE
 <td align="center"><br><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
 <td align="center"><font color="brown"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="orange"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><BR>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
 <td align="center"><font color="blue"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><font color="red"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10"><br>
 <td align="center"><input type="text" id="name" name="name" required
       minlength="4" maxlength="8" size="10">
</tr>
</table>
</center>
<br>
<br>
<div>
<a href="$urlHome" align ="center" class="bouton">Retour à l'accueil</a>
 </div>
   </section>
    <br>
</div>
</body>
END;

        // OK validé pour echo

        echo $html;
    }


    /**
     * Fonction permettant de rendre la vue de suppression de compte.
     *
     */
    public function compteSupprimer()
    {
        $header = VueGenerale::renderHeader();

        $app = \Slim\Slim::getInstance();

        $urlHome = $app->urlFor('route_home');

        $html = <<<END
        $header
<body id="connexion">

    <a href="$urlHome"><img style="height:200px;width:200px" src="$urlHome/img/logo.png"></a>


        <div>

        <p>Votre compte a été supprimé.</p>
        <br>
        <br>
        <a href="$urlHome" class="boutonPPerso">Retour à l'accueil</a>

        </div>
<body>
END;
        //OK validé  pour echo
        echo $html;
    }


    /**
     * Fonction permettant de rendre la vue de modification d'utilisateur.
     *
     */
    public function modification()
    {


        if (isset($_SESSION['session']['user_id'])) {

            $app = \Slim\Slim::getInstance();
            $urlPPersoConfirmerModif = $app->urlFor('route_pagePersoConfirmerModifier');

            $prenom = $_SESSION['session']['prenom'];

            $html = <<<END

    <body>
        <form class="formulaire" method="post" action="$urlPPersoConfirmerModif" enctype="multipart/form-data">
                <h1>Modifier ses informations :</h1><h3> Laissez la valeur par défaut pour conserver la valeur actuelle</h3>

                <input type="text" placeholder="Prenom" name="Prenom" value='$prenom' ><br>
                <input type="password" placeholder="Mot de passe" name="Passe1" ><br>
                <input type="password" placeholder="Confirmation mot de passe" name="Passe2" ><br>
                Choisir une photo de profil :<input type="file" name="image" id="image">
                <input class="bouton" type="submit" value="Valider" required ></input>
        </form>
    </body>
END;
        }


        VueGenerale::renderPage($html, VueGenerale::DarkPage);
    }

    const RECONNEXION = 1;
    const PAGE_PERSO = 2;


    /**
     * Fonction permettant de rendre la vue de confirmation de modification.
     *
     * @param $txt int Permet de choisir une option.
     *
     */
    public function confirmation($txt = self::PAGE_PERSO)
    {

        $app = \Slim\Slim::getInstance();

        $urlHome = $app->urlFor('route_home');

        switch ($txt) {
            case self::RECONNEXION:
                $url = $app->urlFor('route_connexion');
                $info = "Se reconnecter";
                break;

            case self::PAGE_PERSO:
                $url = $app->urlFor('route_get_pagePerso');
                $info = "Retour vers la page personnelle";
                break;
        }

        $html = <<<END

    <a href="$urlHome"><img style="height:200px;width:200px" src="$urlHome/img/logo.png"></a>


        <div>
            <p>Votre compte a été modifié.</p>
            <br>
            <br>
            <a href="$url" class="bouton">$info</a>

        </div>
<body>
END;

        VueGenerale::renderPage($html, VueGenerale::DarkPage);
    }

    /**
     * Fonction permettant de rendre la vue des créateurs.
     *
     * @param $lCreateur Utilisateur[] Liste des créateurs.
     *
     */
    public function afficherCreateur($lCreateur)
    {

        $html = "<div id='listeCreateur'>";
        foreach (array_unique($lCreateur) as $user) {
            $userName = $user->toString();

            $html .=
                "<div class = 'createur'>
                $userName
            </div>";
        }

        $html .= "</div>";

        VueGenerale::renderPage($html, VueGenerale::DarkPage);
    }
}
