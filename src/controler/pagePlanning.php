<?php

namespace wishlist\controler;

use \Illuminate\Database\Capsule\Manager as DB;
use wishlist\model\Liste;
use wishlist\model\Item;
use wishlist\model\User;
use wishlist\model\MessagesListes;
use wishlist\view\VuePagePerso;
use wishlist\authentification\Authentification;
use wishlist\exception\AuthException;
use wishlist\view\VueConfirmation;
use wishlist\view\VuePlanning;

class pagePlanning
{
    public function getPlanning()
    {
        if (isset($_SESSION['session'])) {
            $u = User::where('user_id', '=', $_SESSION['session']['user_id'])->first();
            $v = new VuePlanning();
            $v->render($u);
        } else {
            $c = new IdentifiantControler();
            $c->getConnexion();
        }
    }

}