<?php

namespace wishlist\controler;

use \Illuminate\Database\Capsule\Manager as DB;
use wishlist\model\User;
use wishlist\view\VueIdentifiant;
use wishlist\view\VueConnexion;
use wishlist\view\VueDeconnexion;
use wishlist\authentification\Authentification;
use wishlist\view\VueHome;


class HomeControler
{
    public function getHome($option = "")
    {
        $v = new VueHome();
        $v->render();
    }
}
